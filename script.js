async function getActeurs(){
    let data = await fetch("https://workshop.neotechweb.net/ws/ciamovie/1.0.0/acteurs.php");
    let djson = await data.json();

    let template = document.getElementById("templateActeur");
    let container = document.getElementById("container")
    djson.forEach(acteur => {
        let content = template.content.cloneNode(true);
        content.querySelector(".nom").innerText = acteur.Nom;
        content.querySelector(".portrait>img").src=`https://workshop.neotechweb.net/ws/ciamovie/1.0.0/acteurs/${acteur.idActeur}.jpg`;
        /*let rect = content.getElementById("#film5654");
        console.log(rect);
        rect.querySelector("rect").setAttribute('width', Math.rect.container.appendChild(content));
        */
        container.appendChild(content);
        

        //document.body.appendChild(template.contentEditable.cloneNode(true));
        //console.log(acteur.Nom)
    });
}

async function getFilms(){
    let data = await fetch("https://workshop.neotechweb.net/ws/ciamovie/1.0.0/films.php");
    let djson = await data.json();

    let template = document.getElementById("templateFilms");
    let container = document.getElementById("container2")
    djson.forEach(film => {
        let content = template.content.cloneNode(true);
        content.querySelector(".titre").innerText = film.Titre;
        content.querySelector(".poster>img").src=`https://workshop.neotechweb.net/ws/ciamovie/1.0.0/films/${film.Fichier}`;
        container.appendChild(content);
        

        //document.body.appendChild(template.contentEditable.cloneNode(true));
        //console.log(acteur.Nom)
    });
}

if ('serviceWorker' in navigator){
    window.addEventListener('load', function(){
        navigator.serviceWorker.register('worker.js')
        .then(function(registration){
            console.log('ServiceWorker registration successful with scope: ', 
            registration.scope);
        }, function(err){
            console.log('ServiceWorker registration failed: ', err)
        });
    });
}

getFilms();
getActeurs();
